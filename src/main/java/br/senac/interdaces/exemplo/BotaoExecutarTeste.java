package br.senac.interdaces.exemplo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
                                

public class BotaoExecutarTeste implements ActionListener {
    
    @Override
    public void actionPerformed(ActionEvent e){
        JOptionPane.showMessageDialog(null, "A interface ActionListener, que representa eventos de Ação,"
                + "tal como o clique de um botão \n"
                + "foi implementada pela classe concreta BotaoExecutarTeste!"
                
        );
    }
    
}
