
package br.senac.produto.model;

public class Mercadoria extends Produto {
    
    private Long idMercadoria;
    private byte imagem;

    public Mercadoria() {
    }

    public Mercadoria(Long idMercadoria, byte imagem) {
        this.idMercadoria = idMercadoria;
        this.imagem = imagem;
    }
    
    public Long getIdMercadoria() {
        return idMercadoria;
    }

    public byte getImagem() {
        return imagem;
    }

    public void setIdMercadoria(Long idMercadoria) {
        this.idMercadoria = idMercadoria;
    }

    public void setImagem(byte imagem) {
        this.imagem = imagem;
    }

    @Override
    public Float getTotalPercImposto() {
        return getPercICMS();
    }
    
    
    
    
}
