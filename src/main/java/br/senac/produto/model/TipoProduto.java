package br.senac.produto.model;

/**
 *
 * @author Aluno
 */
public enum TipoProduto {

    MERCADORIA(1, "Mercadoria"),
    MATERIA_PRIMA(2, "Matéria Prima"),
    SERVICO(3, "Serviço");

    private final Integer id;
    private final String nome;
    

    private TipoProduto(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }
}
