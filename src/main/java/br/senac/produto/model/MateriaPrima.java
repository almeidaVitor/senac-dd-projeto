
package br.senac.produto.model;

public class MateriaPrima extends Produto {
    
    private long idMateriaPrima;
    private Float perclPl;

    public MateriaPrima() {
    }

    public MateriaPrima(long idMateriaPrima, Float perclPl) {
        this.idMateriaPrima = idMateriaPrima;
        this.perclPl = perclPl;
    }
    
    public long getIdMateriaPrima() {
        return idMateriaPrima;
    }

    public Float getPerclPl() {
        return perclPl;
    }

    public void setIdMateriaPrima(long idMateriaPrima) {
        this.idMateriaPrima = idMateriaPrima;
    }

    public void setPerclPl(Float perclPl) {
        this.perclPl = perclPl;
    }

    @Override
    public Float getTotalPercImposto() {
        return perclPl + getPercICMS();
    
    
    }


}
