/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.grupoproduto.view;

import br.senac.grupoproduto.model.GrupoProdutoDAO;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import br.senac.produto.model.GrupoProduto;
import br.senac.produto.model.TipoProduto;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Aluno
 */
public class CadGrupoProduto extends JDialog {
    
    public Integer idGrupoProduto;
    
    private GrupoProdutoDAO gpDAO = new GrupoProdutoDAO();
    private GrupoProduto grupoProduto;
    private JTextField txCodigo;
    private JTextField txNome;
    private JRadioButton rbServico;
    private JRadioButton rbMercadoria;
    private JRadioButton rbMatPrima;
    
    private JPanel pnTipoProd = new JPanel();
    
    public static void main(String[] args) {
        
        CadGrupoProduto cad = new CadGrupoProduto(5);
        cad.setVisible(true);
    }
    
    public CadGrupoProduto(Integer idGrupoProduto) {
        configurarTela();
        configurarPanelTipoProduto();
        configurarPanelBotoes();
        configurarPanelDados();
        
        this.idGrupoProduto = idGrupoProduto; //flag
        if (idGrupoProduto == null) { //inclusao
            grupoProduto = new GrupoProduto();
            return;
            
        }
        //alteracao
        grupoProduto = gpDAO.getPorId(idGrupoProduto);
        txCodigo.setText(grupoProduto.getIdGrupoProduto().toString());
        txNome.setText(grupoProduto.getNomeGrupoProduto());
        
        if (grupoProduto.getTipoProduto() == TipoProduto.SERVICO) {
            rbServico.setSelected(true);
        } else if (grupoProduto.getTipoProduto() == TipoProduto.MERCADORIA) {
            rbMercadoria.setSelected(true);
        } else if (grupoProduto.getTipoProduto() == TipoProduto.MATERIA_PRIMA) {
            rbMatPrima.setSelected(true);
        }
        
    }

    CadGrupoProduto() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private void configurarTela() {
        
        this.setTitle("Login");
        this.setLocationRelativeTo(null);
        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setSize(600, 180);
        
    }
    
    private void configurarPanelTipoProduto() {
        
        rbServico = new JRadioButton("Serviço");
        rbMercadoria = new JRadioButton("Mercadoria");
        rbMatPrima = new JRadioButton("Matéria Prima");
        
        pnTipoProd.add(rbServico);
        pnTipoProd.add(rbMercadoria);
        pnTipoProd.add(rbMatPrima);
        
        ButtonGroup bG = new ButtonGroup();
        bG.add(rbServico);
        bG.add(rbMercadoria);
        bG.add(rbMatPrima);
    }
    
    private void configurarPanelBotoes() {
        JPanel pnBotoes = new JPanel();
        
        pnBotoes.setLayout(new FlowLayout(FlowLayout.RIGHT));
        JButton btGravar = new JButton("Gravar");
        btGravar.setSize(new Dimension(0, 30));
        btGravar.setLocation(400, 10);
        pnBotoes.add(btGravar);
        
        pnBotoes.setLayout(new FlowLayout(FlowLayout.RIGHT));
        JButton btCancelar = new JButton("Cancelar");
        btGravar.setSize(new Dimension(0, 30));
        btGravar.setLocation(400, 10);
        pnBotoes.add(btCancelar);
        
        setResizable(false);
        
        this.add(pnBotoes, BorderLayout.PAGE_END);
        
        btGravar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gravar();
                
            }
        });
        
    }
    
    private void configurarPanelDados() {
        
        JLabel lbCodigo = new JLabel("Código: ");
        JLabel lbNome = new JLabel("Nome: ");
        JLabel lbTipo = new JLabel("Tipo: ");
        
        txCodigo = new JTextField();
        txNome = new JPasswordField();
        JTextField txTipo = new JTextField();
        lbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);
        lbNome.setHorizontalAlignment(SwingConstants.RIGHT);
        lbTipo.setHorizontalAlignment(SwingConstants.RIGHT);
        
        JPanel pnDados = new JPanel();
        pnDados.setLayout(new GridLayout(3, 2, 5, 5));
        pnDados.setBorder(new EmptyBorder(4, 4, 4, 4));
        
        pnDados.add(lbCodigo);
        pnDados.add(txCodigo);
        pnDados.add(lbNome);
        pnDados.add(txNome);
        pnDados.add(lbTipo);
        pnDados.add(pnTipoProd);
        
        add(pnDados, BorderLayout.CENTER);
        
    }
    
    public void gravar() {
        if (txNome.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "Nome não informado!", "Erro", JOptionPane.ERROR_MESSAGE);
            txNome.requestFocus();
            return;
        }
        if (rbServico.isSelected() == false && rbMercadoria.isSelected() == false && rbMatPrima.isSelected() == false) {
            JOptionPane.showMessageDialog(this, "Tipo não informado!", "Erro", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        int mensagem = JOptionPane.showConfirmDialog(this, "Confirmar Gravação? ", "Confirmação",
                JOptionPane.OK_CANCEL_OPTION);
        
        if (mensagem != JOptionPane.OK_OPTION) {
            return;
        }
        
        grupoProduto.setNomeGrupoProduto(txNome.getText());
        if (rbServico.isSelected()) {
            grupoProduto.setTipoProduto(TipoProduto.SERVICO);            
        } else if (rbMatPrima.isSelected()) {
            grupoProduto.setTipoProduto(TipoProduto.MATERIA_PRIMA);
        } else if (rbMercadoria.isSelected()) {
            grupoProduto.setTipoProduto(TipoProduto.MERCADORIA);
        }

        //continua RadioButton
        if (grupoProduto.getIdGrupoProduto() == null) {
            Integer idNovo = gpDAO.inserir(grupoProduto);
            JOptionPane.showMessageDialog(this, "Registro conferido com sucesso: " + idNovo, "Sucesso", JOptionPane.INFORMATION_MESSAGE);
            
        } else {
            gpDAO.alterar(grupoProduto);
            JOptionPane.showMessageDialog(this, "Registro alterado com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
        }
        dispose();
    }

    void editar(Integer idGrupoProduto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

/*
       setTitle("Login"); //titulo da tela
        setSize(500, 250); //Largura em pixel, altura 
        
        
        setLocationRelativeTo(null); //centralizar no monitor
        setLayout(null);// Layout absoluto
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); // fechar tela e excluir da memoria RAM
        
        setLayout(new BorderLayout()); // usar o borderlayout 5 areas
        JPanel pnBotoes = new JPanel(); // criar um panel para colocar os botoes
        add(pnBotoes, BorderLayout.SOUTH); // adicionar o panel na parte sul
        pnBotoes.setLayout(null); // o panel tera layout absoluto
        pnBotoes.setPreferredSize(new Dimension(0, 60)); //
        
        
        JButton btGravar = new JButton ("Gravar"); // Instaciar o botao
        btGravar.setSize(new Dimension(80, 30)); // tamanho preferido, largura e altura
        btGravar.setLocation(390, 160); // localizacao na tela 
        pnBotoes.add(btGravar); // adicionar o botao na tela
        
        add(btGravar); // adicionar na tela o botao
        
        JLabel lbLogin = new JLabel("Login: "); // Intanciar um JLabel
        JTextField txLogin = new JTextField(); // Intancia um JTextFild
        lbLogin.setHorizontalAlignment(SwingConstants.RIGHT); // alinhar a direita
        
        JPanel pnDados = new JPanel(); // intanciar um container do tipo JPanel
        pnDados.setLayout(new GridLayout(1, 2, 5, 5)); // uma linha e duas colulas
        pnDados.setBorder(new EmptyBorder(4, 4, 4, 4)); // faz um espaçamento nas bordas do JPanel
        
        pnDados.add(lbLogin);
        pnDados.add(txLogin);
        add(pnDados, BorderLayout.CENTER); // adicoonar o panel no c 
 */
