/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.grupoproduto.model;

import br.senac.produto.model.GrupoProduto;
import br.senac.produto.model.TipoProduto;
import java.util.List;

/**
 *
 * @author Aluno
 */
public class GrupoProdutoDAOTeste {

    public static void main(String[] args) {
        GrupoProdutoDAO dao = new GrupoProdutoDAO();
        
        //Vai gerar uma excecao
        dao.inserir(null);

        GrupoProduto gpNovo = new GrupoProduto();
        gpNovo.setNomeGrupoProduto("Roupas");
        gpNovo.setTipoProduto(TipoProduto.MERCADORIA);
        Integer idNovoGP = dao.inserir(gpNovo);
        System.out.println("Novo id: " + idNovoGP);

        List<GrupoProduto> lista = dao.listarPorNome("f");
        System.out.println(lista);

        for (GrupoProduto gp : lista) {
            System.out.println(gp.getNomeGrupoProduto());
        }
        dao.getMapPorTipoProduto();
    }

}
