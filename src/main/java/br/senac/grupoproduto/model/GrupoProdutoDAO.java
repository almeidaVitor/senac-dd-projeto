package br.senac.grupoproduto.model;

import br.senac.componente.exception.ParametroInvalidosException;
import br.senac.componente.model.BaseDAO;
import br.senac.componente.model.BaseDAO;
import br.senac.produto.model.GrupoProduto;
import br.senac.produto.model.TipoProduto;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class GrupoProdutoDAO implements BaseDAO<GrupoProduto, Integer> {

    private static final ArrayList<GrupoProduto> listaGrupoProduto = new ArrayList<>();

    public GrupoProdutoDAO() { //construtor
        if (listaGrupoProduto.size() > 0) {
            return;
        }
        //MOCK: Objetos para testes
        listaGrupoProduto.add(new GrupoProduto(1, "Alimentos", TipoProduto.MERCADORIA));
        listaGrupoProduto.add(new GrupoProduto(2, "Estética", TipoProduto.SERVICO));
        listaGrupoProduto.add(new GrupoProduto(3, "Higiene", TipoProduto.MERCADORIA));
        listaGrupoProduto.add(new GrupoProduto(4, "Limpeza", TipoProduto.MERCADORIA));
        listaGrupoProduto.add(new GrupoProduto(5, "Cobre", TipoProduto.MATERIA_PRIMA));
        listaGrupoProduto.add(new GrupoProduto(6, "Mecânica", TipoProduto.SERVICO));
        listaGrupoProduto.add(new GrupoProduto(7, "Segurança", TipoProduto.SERVICO));
        listaGrupoProduto.add(new GrupoProduto(8, "Educação", TipoProduto.SERVICO));
        listaGrupoProduto.add(new GrupoProduto(9, "Automóveis", TipoProduto.MERCADORIA));
        listaGrupoProduto.add(new GrupoProduto(10, "Lã", TipoProduto.MATERIA_PRIMA));
        listaGrupoProduto.add(new GrupoProduto(11, "Algodão", TipoProduto.MATERIA_PRIMA));

    }

    @Override
    public GrupoProduto getPorId(Integer id) {
        
       for(GrupoProduto grupoProduto : listaGrupoProduto) {
           if (grupoProduto.getIdGrupoProduto().equals(id)) {
              return grupoProduto;
           }
           
       }
        
        

        return null;
    }

    public boolean excluir(Integer id) {
        int index = 0;
        for (GrupoProduto grupoProduto : listaGrupoProduto) {
            if (grupoProduto.getIdGrupoProduto().equals(id)) {
                listaGrupoProduto.remove(index);
                return true;

            }
        }
        return false;
    }

    public boolean alterar(GrupoProduto objeto) {
        int index = 0;
        for (GrupoProduto grupoProduto : listaGrupoProduto) {
            if (grupoProduto.equals(objeto)) {
                //objeto = grupoProduto.setIdGrupoProduto(ind ex);
                listaGrupoProduto.set(index, objeto);
                return true;
            }
            index++;
        }
        return false;
    }

    public Integer inserir(GrupoProduto objeto) {
        
        if(objeto == null){
            throw new ParametroInvalidosException( "Grupo produto inválido");
        } else if(objeto.getNomeGrupoProduto() == null|| 
                  objeto.getNomeGrupoProduto().equals("")) {
        throw new ParametroInvalidosException ( "Nome dp grupo produto inválido");
    }
        
        int maiorId = 0;
        for (GrupoProduto grupoProd : listaGrupoProduto) {
            if (grupoProd.getIdGrupoProduto() > maiorId) {
                maiorId = grupoProd.getIdGrupoProduto();
            }
        }
        maiorId = maiorId + 1;
        objeto.setIdGrupoProduto(maiorId);
        listaGrupoProduto.add(objeto);
        return maiorId;
    }

    /**
     *
     * @param nome
     * @return
     */
    public List<GrupoProduto> listarPorNome(String nome) {
        List<GrupoProduto> lista = new ArrayList<>();
        //foreach
        for (GrupoProduto objetoGP : listaGrupoProduto) {
            if (objetoGP.getNomeGrupoProduto().toLowerCase().contains(nome.toLowerCase())) {
                lista.add(objetoGP);
            }
        }
        Collections.sort(lista, new Comparator<GrupoProduto>() {
            @Override
            public int compare(GrupoProduto o1, GrupoProduto o2) {
                // nome de forma crescente por "nome e ud"
                int compareStr = o1.getNomeGrupoProduto().compareTo(o2.getNomeGrupoProduto());

                if (compareStr == 0) { //nomes iguais
                    compareStr = o1.getIdGrupoProduto().compareTo(o2.getIdGrupoProduto());

                }

                return compareStr;
            }

        });
        return lista;
    }

    public List<GrupoProduto> listarPorTipo(TipoProduto tipoProduto, String nome) {
        return null;
    }

    public HashMap<TipoProduto, List<GrupoProduto>> getMapPorTipoProduto() {
        // < chave, valor >
        HashMap<TipoProduto, List<GrupoProduto>> map = new HashMap<>();

        if (map.containsKey(TipoProduto.MERCADORIA) == false) {
            map.put(TipoProduto.MERCADORIA, new ArrayList<GrupoProduto>());
        }

        if (map.containsKey(TipoProduto.MATERIA_PRIMA) == false) {
            map.put(TipoProduto.MATERIA_PRIMA, new ArrayList<GrupoProduto>());
        }

        if (map.containsKey(TipoProduto.SERVICO) == false) {
            map.put(TipoProduto.SERVICO, new ArrayList<GrupoProduto>());
        }

        for (GrupoProduto objetoGP : listaGrupoProduto) {
            List<GrupoProduto> lista = map.get(objetoGP.getTipoProduto());
            lista.add(objetoGP);

        }
        System.out.println(map);

        return map;

    }
}
